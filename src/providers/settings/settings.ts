import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {LocalStorageService} from "angular-2-local-storage";
import {Constants} from "../../shared/constants";
import {Observable} from "rxjs";

@Injectable()
export class SettingsProvider {

    constructor(public http: Http, public localStorage: LocalStorageService) {}

    getUserSettings(): any {
        return this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
    }

    updateProfile(userData: any): Observable<any> {
        let userInfo: any = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        return this.http.post(Constants.UPDATE_PROFILE_API + userInfo.id, userData)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    changePassword(passwordData: any): Observable<any> {
        let userInfo: any = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        return this.http.post(Constants.CHANGE_PASSWORD_API + userInfo.id, passwordData)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    deactivate(): Observable<any> {
        let userInfo: any = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        return this.http.post(Constants.DEACTIVATE_ACC_API + userInfo.id, userInfo.id)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }
}
