import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';
import {Constants} from "../../shared/constants";
import {LocalStorageService} from "angular-2-local-storage";

@Injectable()
export class CasesProvider {

    constructor(public http: Http, public localStorage: LocalStorageService) {}

    getAllCases(page: number): Observable<any> {
        return this.http.get(Constants.CASES_API + "?page=" + page)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    getSingleCase(id: number): Observable<any> {
        return this.http.get(Constants.CASES_API + "/" + id)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    getCatCases(id: number, page: number): Observable<any> {
        return this.http.get(Constants.CATEGORY_CASES_API + id + "?page=" + page)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    getMyCases(page: number): Observable<any> {
        let userInfo: any = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        return this.http.get(Constants.USER_CASES_API + userInfo.id + "?page=" + page)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    postCase(caseObject: any): Observable<any> {
        return this.http.post(Constants.CASES_API, JSON.stringify(caseObject))
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    editCase(caseObject: any, id: number): Observable<any> {
        return this.http.put(Constants.CASES_API + "/" + id, JSON.stringify(caseObject))
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    closeCase(id: number): Observable<any> {
        return this.http.post(Constants.CLOSE_CASE_API + id, id)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }
}
