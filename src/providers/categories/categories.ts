import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Constants} from "../../shared/constants";
import {Observable} from "rxjs";

@Injectable()
export class CategoriesProvider {

    constructor(public http: Http) {}

    getCategories(): Observable<any> {
        return this.http.get(Constants.CATEGORIES_API)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }


}
