import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';
import {Constants} from "../../shared/constants";

@Injectable()
export class AboutProvider {

    constructor(public http: Http) {}

    getAboutContent(): Observable<any> {
        return this.http.get(Constants.ABOUT_API)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }
}
