import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';
import {Constants} from "../../shared/constants";

@Injectable()
export class AuthProvider {
    constructor(public http: Http) {}

    login(credentials): Observable<any> {
        return this.http.post(Constants.LOGIN_API, JSON.stringify(credentials))
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    register(credentials): Observable<any> {
        return this.http.post(Constants.REGISTER_API, JSON.stringify(credentials))
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    resetPassword(credentials): Observable<any> {
        return this.http.post(Constants.RESET_PASSWORD_API, JSON.stringify(credentials))
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    getRules(): Observable<any> {
        return this.http.get(Constants.RULES_API)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }
}
