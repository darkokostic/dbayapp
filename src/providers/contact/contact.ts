import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';
import {Constants} from "../../shared/constants";

@Injectable()
export class ContactProvider {

    constructor(public http: Http) {}

    contact(formData: any): Observable<any> {
        return this.http.post(Constants.CONTACT_API, JSON.stringify(formData))
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

}
