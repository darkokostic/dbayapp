import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {Constants} from "../../shared/constants";

@Injectable()
export class ImagesProvider {

    constructor(public http: Http) {}

    upload(images: any, entry_reference: string, id: number): Observable<any> {
        let params = {
            images: images,
            entry_reference: entry_reference,
            user_id: id
        };
        return this.http.post(Constants.ATTACHMENT_API, JSON.stringify(params))
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    deleteImage(id: number): Observable<any> {
        return this.http.delete(Constants.ATTACHMENT_API + "/" + id)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }
}
