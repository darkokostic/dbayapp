import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {AuthProvider} from "../../providers/auth/auth";
import {Toast} from '@ionic-native/toast';
import {LoadingController} from 'ionic-angular';
import {Constants} from "../../shared/constants";
import {LoginPage} from "../login/login";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {

  private form: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public swipeProvider: SwipeProvider, public authProvider: AuthProvider, private toast: Toast, public loadingCtrl: LoadingController) {
      this.form = new FormGroup({
        email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ]))
      });
  }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }

  resetPass(): void {
      if (this.form.valid) {
          let loader = this.loadingCtrl.create({
            content: Constants.PLEASE_WAIT_MESSAGE,
          });
          loader.present();
          this.authProvider.resetPassword(this.form.value)
              .subscribe(
                  (response: any) => {
                      this.form.controls['email'].setValue("");
                      loader.dismiss();
                      this.toast.show(response.msg, '2000', 'center')
                          .subscribe(
                              toast => {
                                  console.log(toast);
                              }
                          );
                  }, (error: any) => {
                      loader.dismiss();
                      if(error.msg) {
                          this.toast.show(error.msg, '2000', 'center')
                              .subscribe(
                                  toast => {
                                      console.log(toast);
                                  }
                              );
                      } else if(error.message) {
                          this.toast.show(error.message, '2000', 'center')
                              .subscribe(
                                  toast => {
                                      console.log(toast);
                                  }
                              );
                      } else {
                          for(let key in error){
                              error[key].forEach(message => {
                                  this.toast.show(message, '2000', 'center')
                                      .subscribe(
                                          toast => {
                                              console.log(toast);
                                          }
                                      );
                              });
                          }
                      }
                  }
              );
          } else if(this.form.value.email == '') {
              this.toast.show(Constants.FILL_EMAIL_FIELD, '2000', 'center')
                  .subscribe(
                      toast => {
                        console.log(toast);
                      }
                  );
          } else {
              this.toast.show(Constants.EMAIL_FIELD_INVALID, '2000', 'center')
                  .subscribe(
                      toast => {
                        console.log(toast);
                      }
                  );
          }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
  }

}
