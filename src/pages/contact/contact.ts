import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {Constants} from "../../shared/constants";
import {LocalStorageService} from "angular-2-local-storage";
import {Toast} from "@ionic-native/toast";
import {ContactProvider} from "../../providers/contact/contact";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-contact',
    templateUrl: 'contact.html',
})
export class ContactPage {
    private form: any;
    private userInfo: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public contactProvider: ContactProvider, public localStorage: LocalStorageService, public loadingCtrl: LoadingController, private toast: Toast, public swipeProvider: SwipeProvider) {
        this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        this.form = new FormGroup({
            subject: new FormControl('', Validators.required),
            message: new FormControl('', Validators.required),
            email: new FormControl('', Validators.required),
            name: new FormControl('', Validators.required)
        });
    }

    ionViewDidEnter() {
        this.form.controls['email'].setValue(this.userInfo.email);
        this.form.controls['name'].setValue(this.userInfo.name);
    }

    contact(): void {
        if(this.form.valid) {
            let loader = this.loadingCtrl.create({
                content: Constants.PLEASE_WAIT_MESSAGE,
            });
            loader.present();
            this.contactProvider.contact(this.form.value)
                .subscribe(
                    (response: any) => {
                        loader.dismiss();
                        this.setDefaultValues();
                        console.log(response);
                        this.toast.show(response, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    }, (error: any) => {
                        loader.dismiss();
                        if(error.msg) {
                            this.toast.show(error, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else if(error.message) {
                            this.toast.show(error.message, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            for(let key in error){
                                error[key].forEach(message => {
                                    this.toast.show(message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                });
                            }
                        }
                    }
                );
        } else {
            this.toast.show(Constants.FILL_ALL_FIELDS_MESSAGE, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        }
    }

    setDefaultValues(): void {
        this.form.controls['subject'].setValue("");
        this.form.controls['message'].setValue("");
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
