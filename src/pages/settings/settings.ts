import {MyCasesPage} from './../my-cases/my-cases';
import {MyDonationsPage} from './../my-donations/my-donations';
import {MessagesPage} from './../messages/messages';
import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController, App} from 'ionic-angular';
import {SettingsProvider} from "../../providers/settings/settings";
import {CitiesProvider} from "../../providers/cities/cities";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {Constants} from "../../shared/constants";
import {Toast} from "@ionic-native/toast";
import {LocalStorageService} from "angular-2-local-storage";
import {LoginPage} from "../login/login";
import {TabsTransitionProvider} from "../../providers/tabs-transition/tabs-transition";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html',
})
export class SettingsPage {

    private userSettings: any = {};
    private cities: any;
    private form: any;
    private passwordForm: any;
    private canDisableAccount: boolean = false;
    public userInfo: any = {};

    constructor(public app: App, public navCtrl: NavController, public swipeProvider: SwipeProvider, public navParams: NavParams, public settingsProvider: SettingsProvider, private citiesProvider: CitiesProvider, private toast: Toast, public loadingCtrl: LoadingController, public localStorage: LocalStorageService, public tabsTransitionProvider: TabsTransitionProvider) {
        this.form = new FormGroup({
            name: new FormControl('', Validators.required),
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            city: new FormControl('', Validators.required),
            area: new FormControl('', Validators.required),
            street: new FormControl('', Validators.required),
            phone: new FormControl('', Validators.required),
        });

        this.passwordForm = new FormGroup({
            old_password: new FormControl('', Validators.required),
            password: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(7)
            ])),
            password_confirmation: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(7)
            ])),
        });
    }

    SettingsPage = SettingsPage;
    MessagesPage = MessagesPage;
    MyDonationsPage = MyDonationsPage;
    MyCasesPage = MyCasesPage;
    login_page = LoginPage;

    goToPage(pageName) {
        this.navCtrl.setRoot(pageName);
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }

    ionViewDidEnter() {
        if(this.tabsTransitionProvider.getTab()) {
            this.navCtrl.push(this.tabsTransitionProvider.getTab());
            this.tabsTransitionProvider.setTab(null);
        } else {
            this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
            this.citiesProvider.getCities()
                .subscribe(
                    (response: any) => {
                        this.cities = response;
                    }, (error: any) => {
                        if(error.msg) {
                            this.toast.show(error.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else if(error.message) {
                            this.toast.show(error.message, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            for(let key in error){
                                error[key].forEach(message => {
                                    this.toast.show(message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                });
                            }
                        }
                    }
                );
            this.userSettings = this.settingsProvider.getUserSettings();
            this.form = new FormGroup({
                name: new FormControl(this.userSettings.name, Validators.required),
                email: new FormControl(this.userSettings.email, Validators.compose([
                    Validators.required,
                    Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
                ])),
                city: new FormControl(this.userSettings.city, Validators.required),
                area: new FormControl(this.userSettings.area, Validators.required),
                street: new FormControl(this.userSettings.street, Validators.required),
                phone: new FormControl(this.userSettings.phone, Validators.required),
            });
        }
    }

    editProfile(): void {
        if(this.form.valid) {
            let loader = this.loadingCtrl.create({
                content: Constants.PLEASE_WAIT_MESSAGE,
            });
            loader.present();
            this.settingsProvider.updateProfile(this.form.value)
                .subscribe(
                    (response: any) => {
                        loader.dismiss();
                        if (!response.msg) {
                            this.userSettings = response;
                            this.localStorage.set(Constants.USER_INFO_LOCALSTORAGE, response);
                            this.toast.show(Constants.UPDATED_USER_PROFILE_MESSAGE, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            this.toast.show(response.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        }
                    }, (error: any) => {
                        loader.dismiss();
                        if(error.msg) {
                            this.toast.show(error.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else if(error.message) {
                            this.toast.show(error.message, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            for(let key in error){
                                error[key].forEach(message => {
                                    this.toast.show(message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                });
                            }
                        }
                    }
                );
        } else if (this.form.value.name == '' || this.form.value.email == '' || this.form.value.city == '' || this.form.value.phone == '' || this.form.value.area == '' || this.form.value.street == '') {
            this.toast.show(Constants.FILL_ALL_FIELDS_MESSAGE, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        } else {
            this.toast.show(Constants.EMAIL_FIELD_INVALID, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        }
    }

    changePassword(): void {
        if(this.passwordForm.valid) {
            if(this.passwordForm.value.password != this.passwordForm.value.password_confirmation) {
                this.toast.show(Constants.PASSWORDS_MUST_MATCH_MESSAGE, '2000', 'center')
                    .subscribe(
                        toast => {
                            console.log(toast);
                        }
                    );
            } else {
                let loader = this.loadingCtrl.create({
                    content: Constants.PLEASE_WAIT_MESSAGE,
                });
                loader.present();
                this.settingsProvider.changePassword(this.passwordForm.value)
                    .subscribe(
                        (response: any) => {
                            loader.dismiss();
                            this.toast.show(response.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                            this.passwordForm.controls['old_password'].setValue("");
                            this.passwordForm.controls['password'].setValue("");
                            this.passwordForm.controls['password_confirmation'].setValue("");
                        }, (error: any) => {
                            loader.dismiss();
                            if(error.msg) {
                                this.toast.show(error.msg, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            } else if(error.message) {
                                this.toast.show(error.message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            } else {
                                for(let key in error){
                                    error[key].forEach(message => {
                                        this.toast.show(message, '2000', 'center')
                                            .subscribe(
                                                toast => {
                                                    console.log(toast);
                                                }
                                            );
                                    });
                                }
                            }
                        }
                    );
            }
        } else if(this.passwordForm.value.old_password == '' || this.passwordForm.value.password == '' || this.passwordForm.value.password_confirmation == '') {
            this.toast.show(Constants.FILL_ALL_FIELDS_MESSAGE, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        } else if(this.passwordForm.value.password.length < 7 || this.passwordForm.value.password_confirmation.length < 7) {
            this.toast.show(Constants.PASSWORD_LENGTH_MESSAGE, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        }
    }

    deactivateProfile(canDisableAccount: boolean): void {
        if(canDisableAccount) {
            this.settingsProvider.deactivate()
                .subscribe(
                    (response: any) => {
                        this.toast.show(response.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                        this.localStorage.remove(Constants.USER_INFO_LOCALSTORAGE);
                        this.app.getRootNav().setRoot(LoginPage);
                    }, (error: any) => {
                        if(error.msg) {
                            this.toast.show(error.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else if(error.message) {
                            this.toast.show(error.message, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            for(let key in error){
                                error[key].forEach(message => {
                                    this.toast.show(message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                });
                            }
                        }
                    }
                );
        } else {
            this.toast.show(Constants.AGREE_DEACTIVATE_PROFILE_MESSAGE, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        }
    }

}
