import {CasePage} from './../case/case';
import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {CasesProvider} from "../../providers/cases/cases";
import {Constants} from "../../shared/constants";
import {LocalStorageService} from "angular-2-local-storage";
import {Toast} from "@ionic-native/toast";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-case-category',
    templateUrl: 'case-category.html',
})
export class CaseCategoryPage {

    private category: any;
    private cases: any = [];
    private userInfo: any;
    private area: any;
    private city: any;
    private page: number;
    private hasMore: boolean;

    constructor(public navCtrl: NavController, public navParams: NavParams, private casesProvider: CasesProvider, public localStorage: LocalStorageService, public loadingCtrl: LoadingController, private toast: Toast, public swipeProvider: SwipeProvider) {
        this.category = this.navParams.data;
        this.page = 1;
        this.hasMore = true;
        this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        this.area = this.userInfo.area;
        this.city = this.userInfo.city;
    }


    goToCase(id) {
        this.navCtrl.push(CasePage, {id: id});
    }

    ionViewDidEnter() {
        let loader = this.loadingCtrl.create({
            content: Constants.LOADING_DATA_MESSAGE,
        });
        loader.present();
        this.getCatCases(loader, null);
    }

    loadMore(event: any): void {
        this.getCatCases(null, event);
    }

    sync(event: any) {
        this.page = 1;
        this.cases = [];
        this.getCatCases(null, event);
    }

    getCatCases(loader: any, infiniteScroll: any): void {
        this.casesProvider.getCatCases(this.category.id, this.page)
            .subscribe(
                (response: any) => {
                    response.data.forEach(caseItem => {
                        this.cases.push(caseItem);
                    });
                    this.page++;
                    if(this.page > response.last_page) {
                        this.hasMore = false;
                    }
                    if(loader) {
                        loader.dismiss();
                    }
                    if(infiniteScroll) {
                        infiniteScroll.complete();
                    }
                }, (error: any) => {
                    if(loader) {
                        loader.dismiss();
                    }
                    if(infiniteScroll) {
                        infiniteScroll.complete();
                    }
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
