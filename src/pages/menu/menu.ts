import {TermsPage} from './../terms/terms';
import {LoginPage} from './../login/login';
import {CasePage} from './../case/case';
import {AboutPage} from './../about/about';
import {Component} from '@angular/core';
import {NavController, NavParams, App} from 'ionic-angular';
import {LocalStorageService} from "angular-2-local-storage";
import {Constants} from "../../shared/constants";
import {FaqPage} from "../faq/faq";
import {ContactPage} from "../contact/contact";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-menu',
    templateUrl: 'menu.html',
})
export class MenuPage {

    about_page = AboutPage;
    case_page = CasePage;
    login_page = LoginPage;
    terms_page = TermsPage;
    faq_page = FaqPage;
    contact_page = ContactPage;

    public userInfo: any = {};

    constructor(public app: App, public navCtrl: NavController, public swipeProvider: SwipeProvider, public navParams: NavParams, public localStorage: LocalStorageService) {
    }

    goToPage(page) {
        this.navCtrl.push(page);
    }

    ionViewDidLoad() {
        this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
    }

    logout(): void {
        this.localStorage.remove(Constants.USER_INFO_LOCALSTORAGE);
        this.app.getRootNav().setRoot(LoginPage);
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
