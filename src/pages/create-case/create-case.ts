import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController, ActionSheetController} from 'ionic-angular';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {CasesProvider} from "../../providers/cases/cases";
import {Constants} from "../../shared/constants";
import {Toast} from "@ionic-native/toast";
import {LocalStorageService} from "angular-2-local-storage";
import {CitiesProvider} from "../../providers/cities/cities";
import {CategoriesProvider} from "../../providers/categories/categories";
import {ImagesProvider} from "../../providers/images/images";
import {MyCasesPage} from "../my-cases/my-cases";
import {TabsTransitionProvider} from "../../providers/tabs-transition/tabs-transition";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-create-case',
    templateUrl: 'create-case.html',
})
export class CreateCasePage {
    private whichAddress: any = 1;
    private cities: any = [];
    private userInfo: any;
    private form: FormGroup;
    private categories: any;
    private images: any = [];
    private loader: any;
    private playerId: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public camera: Camera, public casesProvider: CasesProvider, private toast: Toast, public localStorage: LocalStorageService, private citiesProvider: CitiesProvider, private categoriesProvider: CategoriesProvider, public loadingCtrl: LoadingController, private imagesProvider: ImagesProvider, public actionSheetCtrl: ActionSheetController, public tabsTransitionProvider: TabsTransitionProvider, public swipeProvider: SwipeProvider) {
        this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        this.playerId = this.localStorage.get(Constants.ONESIGNAL_ID_KEY);
        this.form = new FormGroup({
            player_id: new FormControl(this.playerId),
            title: new FormControl('', Validators.required),
            details: new FormControl('', Validators.required),
            category: new FormControl('', Validators.required),
            address_city: new FormControl('', Validators.required),
            address_area: new FormControl('', Validators.required),
            address_street: new FormControl('', Validators.required),
            user: new FormControl(this.userInfo.id),
            entry_reference: new FormControl(this.randomString()),
            same_address: new FormControl(this.whichAddress)
        });
    }

    chooseAddressType(type: number): void {
        this.whichAddress = type;
        if(!this.whichAddress) {
            this.form.controls['address_city'].setValue("");
            this.form.controls['address_area'].setValue("");
            this.form.controls['address_street'].setValue("");
            this.citiesProvider.getCities()
                .subscribe(
                    (response: any) => {
                        this.cities = response;
                        this.form.controls['address_city'].setValue(this.cities[0].id);
                    }, (error: any) => {
                        if(error.msg) {
                            this.toast.show(error.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else if(error.message) {
                            this.toast.show(error.message, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            for(let key in error){
                                error[key].forEach(message => {
                                    this.toast.show(message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                });
                            }
                        }
                    }
                );
        }
    }

    createCase(): void {
        if(this.whichAddress) {
            this.form.controls['address_city'].setValue(this.userInfo.city);
            this.form.controls['address_area'].setValue(this.userInfo.area);
            this.form.controls['address_street'].setValue(this.userInfo.street);
        }
        if(this.form.valid) {
            this.loader = this.loadingCtrl.create({
                content: Constants.PLEASE_WAIT_MESSAGE,
            });
            this.loader.present();
            this.casesProvider.postCase(this.form.value)
                .subscribe(
                    (response: any) => {
                        if (!response.msg) {
                            if(this.images.length != 0) {
                                this.uploadImages();
                            } else {
                                this.loader.dismiss();
                                this.setDefaultValues();
                                this.toast.show(Constants.CREATED_CASE_MESSAGE, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                                this.tabsTransitionProvider.setTab(MyCasesPage);
                                this.navCtrl.parent.select(3);
                            }
                        } else {
                            this.loader.dismiss();
                            this.toast.show(response.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        }
                    }, (error: any) => {
                        this.loader.dismiss();
                        if(error.msg) {
                            this.toast.show(error.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else if(error.message) {
                            this.toast.show(error.message, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            for(let key in error){
                                error[key].forEach(message => {
                                    this.toast.show(message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                });
                            }
                        }
                    }
                )
        } else {
            this.toast.show(Constants.FILL_ALL_FIELDS_MESSAGE, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        }
    }

    uploadImages(): void {
        this.imagesProvider.upload(this.images, this.form.value.entry_reference, this.userInfo.id)
            .subscribe(
                (response: any) => {
                    this.loader.dismiss();
                    this.setDefaultValues();
                    this.toast.show(Constants.CREATED_CASE_MESSAGE, '2000', 'center')
                        .subscribe(
                            toast => {
                                console.log(toast);
                            }
                        );
                    this.tabsTransitionProvider.setTab(MyCasesPage);
                    this.navCtrl.parent.select(3);
                }, (error: any) => {
                    this.loader.dismiss();
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            )
    }

    takePhoto(): void {
        let imageOptions: any;
        let actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: Constants.TAKE_A_PICTURE_LABEL,
                    role: 'destructive',
                    handler: () => {
                        imageOptions = {
                            quality: 35,
                            targetWidth: 1000,
                            targetHeight: 1000,
                            sourceType: this.camera.PictureSourceType.CAMERA,
                            encodingType: this.camera.EncodingType.JPEG,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true,
                            saveToPhotoAlbum: true
                        };
                        this.getPicture(imageOptions);
                    }
                },{
                    text: Constants.CHOOSE_FROM_LIBRARY_LABEL,
                    role: 'destructive',
                    handler: () => {
                        imageOptions = {
                            quality: 35,
                            targetWidth: 1000,
                            targetHeight: 1000,
                            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                            encodingType: this.camera.EncodingType.JPEG,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true
                        };
                        this.getPicture(imageOptions);
                    }
                },{
                    text: Constants.CANCEL_LABEL,
                    role: 'cancel',
                    handler: () => {
                    }
                }
            ]
        });
        actionSheet.present();
    }

    getPicture(imageOptions: any): void {
        this.camera.getPicture(imageOptions)
            .then((imageData: any) => {
                this.images.push("data:image/jpeg;base64," + imageData);
            });
    }

    setDefaultValues(): void {
        this.form.controls['address_city'].setValue("");
        this.form.controls['address_area'].setValue("");
        this.form.controls['address_street'].setValue("");
        this.form.controls['title'].setValue("");
        this.form.controls['details'].setValue("");
        this.form.controls['category'].setValue(1);
        this.form.controls['same_address'].setValue(1);
        this.form.controls['entry_reference'].setValue(this.randomString());
        this.chooseAddressType(1);
        this.images = [];
    }

    ionViewDidEnter() {
        this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        this.playerId = this.localStorage.get(Constants.ONESIGNAL_ID_KEY);
        this.form = new FormGroup({
            player_id: new FormControl(this.playerId),
            title: new FormControl('', Validators.required),
            details: new FormControl('', Validators.required),
            category: new FormControl('', Validators.required),
            address_city: new FormControl('', Validators.required),
            address_area: new FormControl('', Validators.required),
            address_street: new FormControl('', Validators.required),
            user: new FormControl(this.userInfo.id),
            entry_reference: new FormControl(this.randomString()),
            same_address: new FormControl(this.whichAddress)
        });
        this.categoriesProvider.getCategories()
            .subscribe(
                (response: any) => {
                    this.categories = response;
                    this.form.controls['category'].setValue(this.categories[0].id);
                }, (error: any) => {
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    randomString(): string {
        let chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        let result = '';
        for (let i = 32; i > 0; --i) {
            result += chars[Math.floor(Math.random() * chars.length)];
        }
        return result;
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
