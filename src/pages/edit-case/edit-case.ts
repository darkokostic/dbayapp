import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController, ActionSheetController, AlertController} from 'ionic-angular';
import {FormGroup, Validators, FormControl} from "@angular/forms";
import {Constants} from "../../shared/constants";
import {CasesProvider} from "../../providers/cases/cases";
import {Toast} from "@ionic-native/toast";
import {LocalStorageService} from "angular-2-local-storage";
import {CitiesProvider} from "../../providers/cities/cities";
import {Camera} from "@ionic-native/camera";
import {CategoriesProvider} from "../../providers/categories/categories";
import {ImagesProvider} from "../../providers/images/images";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-edit-case',
    templateUrl: 'edit-case.html',
})
export class EditCasePage {

    private whichAddress: any;
    private cities: any = [];
    private userInfo: any;
    private form: FormGroup;
    private categories: any;
    private endpoint: string;
    private newImages: any = [];
    private oldImages: any;
    private loader: any;
    private deletedImages: any = [];
    private caseInfo: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public camera: Camera, public swipeProvider: SwipeProvider, public casesProvider: CasesProvider, private toast: Toast, public localStorage: LocalStorageService, private citiesProvider: CitiesProvider, private categoriesProvider: CategoriesProvider, public loadingCtrl: LoadingController, private imagesProvider: ImagesProvider, public actionSheetCtrl: ActionSheetController, public alertCtrl: AlertController) {
        this.endpoint = Constants.IMAGES_ENDPOINT;
        this.whichAddress = 1;
        this.form = new FormGroup({
            title: new FormControl("", Validators.required),
            details: new FormControl("", Validators.required),
            category: new FormControl("", Validators.required),
            address_city: new FormControl("", Validators.required),
            address_area: new FormControl("", Validators.required),
            address_street: new FormControl("", Validators.required),
            user: new FormControl(""),
            entry_reference: new FormControl(""),
            same_address: new FormControl("")
        });
    }

    ionViewDidEnter() {
        this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        let loader = this.loadingCtrl.create({
            content: Constants.LOADING_DATA_MESSAGE,
        });
        loader.present();
        this.casesProvider.getSingleCase(this.navParams.data.id)
            .subscribe(
                (response: any) => {
                    this.caseInfo = response[0];
                    this.oldImages = response[0].attachments;
                    if(response[0].same_address) {
                        this.whichAddress = 1;
                        this.form = new FormGroup({
                            title: new FormControl(response[0].title, Validators.required),
                            details: new FormControl(response[0].details, Validators.required),
                            category: new FormControl(response[0].category, Validators.required),
                            address_city: new FormControl(this.userInfo.city, Validators.required),
                            address_area: new FormControl(this.userInfo.area, Validators.required),
                            address_street: new FormControl(this.userInfo.street, Validators.required),
                            user: new FormControl(response[0].user),
                            entry_reference: new FormControl(response[0].entry_reference),
                            same_address: new FormControl(this.whichAddress)
                        });
                    } else {
                        this.whichAddress = 0;
                        this.form = new FormGroup({
                            title: new FormControl(response[0].title, Validators.required),
                            details: new FormControl(response[0].details, Validators.required),
                            category: new FormControl(response[0].category, Validators.required),
                            address_city: new FormControl(response[0].address_city, Validators.required),
                            address_area: new FormControl(response[0].address_area, Validators.required),
                            address_street: new FormControl(response[0].address_street, Validators.required),
                            user: new FormControl(response[0].user),
                            entry_reference: new FormControl(response[0].entry_reference),
                            same_address: new FormControl(this.whichAddress)
                        });
                    }
                    loader.dismiss();
                }, (error: any) => {
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );

        this.getCities();
        this.categoriesProvider.getCategories()
            .subscribe(
                (response: any) => {
                    this.categories = response;
                    this.form.controls['category'].setValue(this.navParams.data.category);
                }, (error: any) => {
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    chooseAddressType(type: number): void {
        this.whichAddress = type;
        if(!this.whichAddress) {
            this.getCities();
        }
    }

    getCities(): void {
        this.citiesProvider.getCities()
            .subscribe(
                (response: any) => {
                    this.cities = response;
                }, (error: any) => {
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    editCase(): void {
        if(this.whichAddress) {
            this.form.controls['address_city'].setValue(this.userInfo.city);
            this.form.controls['address_area'].setValue(this.userInfo.area);
            this.form.controls['address_street'].setValue(this.userInfo.street);
            this.form.controls['same_address'].setValue(1);
        } else {
            this.form.controls['same_address'].setValue(0);
        }
        if(this.form.valid) {
            this.loader = this.loadingCtrl.create({
                content: Constants.PLEASE_WAIT_MESSAGE,
            });
            this.loader.present();
            this.casesProvider.editCase(this.form.value, this.navParams.data.id)
                .subscribe(
                    (response: any) => {
                        if (!response.msg) {
                            if(this.deletedImages.length != 0) {
                                this.callDeleteImages();
                            } else if(this.newImages.length != 0) {
                                this.uploadImages();
                            } else {
                                this.loader.dismiss();
                                this.toast.show(Constants.EDITED_CASE_MESSAGE, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            }
                        } else {
                            this.loader.dismiss();
                            this.toast.show(response.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        }
                    }, (error: any) => {
                        this.loader.dismiss();
                        if(error.msg) {
                            this.toast.show(error.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else if(error.message) {
                            this.toast.show(error.message, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            for(let key in error){
                                error[key].forEach(message => {
                                    this.toast.show(message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                });
                            }
                        }
                    }
                )
        } else {
            this.toast.show(Constants.FILL_ALL_FIELDS_MESSAGE, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        }
    }

    callDeleteImages(): void {
        for(let i = 0; i < this.deletedImages.length; i++) {
            this.imagesProvider.deleteImage(this.deletedImages[i].id)
                .subscribe(
                    (response: any) => {
                        if(i == (this.deletedImages.length - 1)) {
                            if(this.newImages.length != 0) {
                                this.uploadImages();
                            } else {
                                this.loader.dismiss();
                                this.toast.show(Constants.EDITED_CASE_MESSAGE, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            }
                        }
                    }, (error: any) => {
                        this.loader.dismiss();
                        if(error.msg) {
                            this.toast.show(error.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else if(error.message) {
                            this.toast.show(error.message, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            for(let key in error){
                                error[key].forEach(message => {
                                    this.toast.show(message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                });
                            }
                        }
                    }
                )
        }
    }

    uploadImages(): void {
        this.imagesProvider.upload(this.newImages, this.form.value.entry_reference, this.userInfo.id)
            .subscribe(
                (response: any) => {
                    this.newImages = [];
                    this.casesProvider.getSingleCase(this.navParams.data.id)
                        .subscribe(
                            (response: any) => {
                                this.oldImages = response[0].attachments;
                                this.loader.dismiss();
                                this.toast.show(Constants.EDITED_CASE_MESSAGE, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            }
                        )
                }, (error: any) => {
                    this.loader.dismiss();
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            )
    }

    deleteImage(image: any, index: number): void {
        let confirm = this.alertCtrl.create({
            title: Constants.DELETE_IMAGE_TITLE,
            message: Constants.DELETE_IMAGE_QUESTION,
            buttons: [
                {
                    text: Constants.DELETE_IMAGE_CANCEL_LABEL,
                    handler: () => {}
                },
                {
                    text: Constants.DELETE_IMAGE_YES_LABEL,
                    handler: () => {
                        this.oldImages.splice(index, 1);
                        this.deletedImages.push(image);
                    }
                }
            ]
        });
        confirm.present();
    }

    takePhoto(): void {
        let imageOptions: any;
        let actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: Constants.TAKE_A_PICTURE_LABEL,
                    role: 'destructive',
                    handler: () => {
                        imageOptions = {
                            quality: 35,
                            targetWidth: 1000,
                            targetHeight: 1000,
                            sourceType: this.camera.PictureSourceType.CAMERA,
                            encodingType: this.camera.EncodingType.JPEG,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true,
                            saveToPhotoAlbum: true
                        };
                        this.getPicture(imageOptions);
                    }
                },{
                    text: Constants.CHOOSE_FROM_LIBRARY_LABEL,
                    role: 'destructive',
                    handler: () => {
                        imageOptions = {
                            quality: 35,
                            targetWidth: 1000,
                            targetHeight: 1000,
                            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                            encodingType: this.camera.EncodingType.JPEG,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true
                        };
                        this.getPicture(imageOptions);
                    }
                },{
                    text: Constants.CANCEL_LABEL,
                    role: 'cancel',
                    handler: () => {
                    }
                }
            ]
        });
        actionSheet.present();
    }

    getPicture(imageOptions: any): void {
        this.camera.getPicture(imageOptions)
            .then((imageData: any) => {
                this.newImages.push("data:image/jpeg;base64," + imageData);
            });
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}