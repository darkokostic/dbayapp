import {MessagePage} from './../message/message';
import {MyCasesPage} from './../my-cases/my-cases';
import {MyDonationsPage} from './../my-donations/my-donations';
import {SettingsPage} from './../settings/settings';
import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {MessagesProvider} from "../../providers/messages/messages";
import {Constants} from "../../shared/constants";
import {LocalStorageService} from "angular-2-local-storage";
import {Toast} from "@ionic-native/toast";
import {TabsTransitionProvider} from "../../providers/tabs-transition/tabs-transition";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-messages',
    templateUrl: 'messages.html',
})
export class MessagesPage {

    private messages: any = [];
    private message: string;
    public userInfo: any = {};

    constructor(public navCtrl: NavController, public navParams: NavParams, public swipeProvider: SwipeProvider, public messagesprovider: MessagesProvider, public loadingCtrl: LoadingController, public localStorage: LocalStorageService, private toast: Toast, public tabsTransitionProvider: TabsTransitionProvider) {

    }

    SettingsPage = SettingsPage;
    MessagesPage = MessagesPage;
    MyDonationsPage = MyDonationsPage;
    MyCasesPage = MyCasesPage;
    MessagePage = MessagePage;

    pushToPage(messageObject) {
        this.navCtrl.push(MessagePage, messageObject);
    }

    goToPage(pageName) {
        this.navCtrl.setRoot(pageName);
    }


    ionViewDidEnter() {
        if(this.tabsTransitionProvider.getTab()) {
            this.navCtrl.push(this.tabsTransitionProvider.getTab());
            this.tabsTransitionProvider.setTab(null);
        } else {
            this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
            let loader = this.loadingCtrl.create({
                content: Constants.LOADING_DATA_MESSAGE,
            });
            loader.present();
            this.messagesprovider.getUserMessages()
                .subscribe(
                    (response: any) => {
                        this.messages = response.msg_subject_container;
                        this.message = Constants.MY_MESSAGES_MESSAGE;
                        loader.dismiss();
                    }, (error: any) => {
                        loader.dismiss();
                        if(error.msg) {
                            this.toast.show(error.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else if(error.message) {
                            this.toast.show(error.message, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            for(let key in error){
                                error[key].forEach(message => {
                                    this.toast.show(message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                });
                            }
                        }
                    }
                );
        }
    }

    sync(event: any) {
        this.messagesprovider.getUserMessages()
            .subscribe(
                (response: any) => {
                    this.messages = response.msg_subject_container;
                    this.message = Constants.MY_MESSAGES_MESSAGE;
                    event.complete();
                }, (error: any) => {
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
