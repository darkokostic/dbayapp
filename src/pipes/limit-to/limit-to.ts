import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the LimitToPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'limitTo',
})
export class LimitToPipe implements PipeTransform {
    transform(value: string, limit: number) : string {
        let dots = '...';
        if(value.length > limit) {
            return value.substring(0, limit) + dots;
        } else {
            return value;
        }
    }
}
